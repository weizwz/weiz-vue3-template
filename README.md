# weiz-vue3-template

## 简介

Vite4+Typescript+Vue3+Pinia 项目模板

## 运行

安装依赖
```shell
npm i
```

运行
```shell
npm run dev
```

打包
```shell
npm run build
```